############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2014 Xilinx Inc. All rights reserved.
############################################################
open_project fir
set_top fir
add_files fir.c
add_files fir.h
open_solution "solution1"
set_part {xc6slx45tfgg484-3}
create_clock -period 10 -name default
source "./fir/solution1/directives.tcl"
#csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
